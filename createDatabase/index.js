const sqlite = require("sqlite3").verbose();
const fs = require("fs");

const STANDARD_SETS = ["xln", "rix", "dom", "m19", "grn"];

const db = new sqlite.Database("./mtg.sqlite", err => {
  if (err) {
    console.error(err);
  }
});

function createTables() {
  const sqlString = fs.readFileSync("create-table.sql", { encoding: "UTF-8" });
  const statements = sqlString.split(";").filter(s => s.length > 0);
  for (const statement of statements) {
    const st = statement.trim();
    db.run(st, err => {
      if (err) {
        console.error(err);
      } else {
        console.log("Beginning inserts");
      }
    });
  }
}

function insertData() {
  const text = fs.readFileSync("scryfall-default-cards.json", {
    encoding: "UTF-8"
  });
  const cards = JSON.parse(text);

  db.run("begin transaction");
  cards.forEach(card => {
    const {
      name,
      id,
      set,
      colors,
      cmc,
      type_line,
      image_uris,
    } = card;
    const colorString = colors !== undefined ? colors.join("") : "";
    const isInStandard = STANDARD_SETS.includes(set);  
    
    if (isInStandard) {
      let image;
      if (image_uris) {
        image = image_uris.png;
      } else if (card.card_faces) {
        image = card.card_faces.map(c => c.image_uris.png).join(" ");
      }

      const params = [id, name, image, set, colorString, cmc, type_line];
      db.run(
        "INSERT INTO cards (id, \"name\", image_url_png, set_name, colors, converted_mana_cost, type_line) VALUES (?, ?, ?, ?, ?, ?, ?)",
        params,
        function (err) {
          if (err) {
            console.error("error", err, "with card", params);
            process.exit(1);
          } else {
            console.log("Inserted card " + name + " with ID " + this.lastID);
          }
        }
      );
    }
  });
  db.run("commit");
}

db.serialize(() => {
  createTables();
  insertData();
})