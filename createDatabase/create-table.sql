CREATE TABLE cards (
    id VARCHAR(36) NOT NULL PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    image_url_png VARCHAR NOT NULL,
    set_name VARCHAR,
    colors VARCHAR,
    converted_mana_cost INTEGER NOT NULL,
    type_line VARCHAR NOT NULL
);