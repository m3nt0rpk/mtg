import React, {Component} from 'react';
import {View, Image, Text, TouchableHighlight} from 'react-native';
import styles from '../styles'
import {Linking} from 'react-native';
import {WebBrowser} from 'expo';

class Home extends Component {
    static navigationOptions = {
      drawerLabel: "Home"
    }

    handleReactLink= () => {
      Linking.openURL('https://magic.wizards.com/en');
    }
    handleExpoLink = () => {
      WebBrowser.openBrowserAsync('https://www.mtggoldfish.com/');
    } 
  
    render() {
      return (
        <View style={styles.home}>
          <Text style={styles.homeText}>Press the image for more information about MTG!</Text>
          <TouchableHighlight onPress={this.handleReactLink}>
            <Image source={{uri: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Magicthegathering-logo.svg/2000px-Magicthegathering-logo.svg.png"}} style={styles.logo}/>
          </TouchableHighlight>
          <Text style={styles.homeText}>Press the image for actual meta decks!</Text>
          <TouchableHighlight onPress={this.handleExpoLink}>
            <Image source={{uri: "https://static-cdn.jtvnw.net/jtv_user_pictures/mtggoldfish-profile_image-75307ca625487f11-300x300.png"}} style={styles.goldfish} />
          </TouchableHighlight>
        </View>
      );
    }
  }

export default Home;