import React, { Component } from "react";
import {
  View,
  Image,
  FlatList,
  TouchableHighlight,
} from "react-native";
import Icon from "@expo/vector-icons/Ionicons";
import {HeaderButtons, SearchInput} from './helpers.js'
import styles from '../styles';
import {openDb, getCards, getCardsOnMount, getSingleCard} from '../db';

class CardList extends Component {
    static navigationOptions = {
      drawerLabel: "Guilds of Ravnica"
    }
    constructor(props) {
      super(props);
      this.state = { 
        cards: [],
        page: 0,
        searchText: ""
      };
    }
  
    componentDidMount = async () => {
      const db = await openDb();
      this.db = db;
      this.getAdditionalData()
    }
  
    getAdditionalData = async () => {
      const page = this.state.page
      try{
        const cards = await getCardsOnMount(this.props.set, page, this.db)
        this.setState({
          cards: [...this.state.cards, ...cards],
          page: page + 1,
          filter: false
        });
      }
      catch(err){
        console.log(err)
      }
    };
  
    getCards = async (color) => {
      try{
      const cards = await getCards(this.props.set, color, this.db)
        this.setState({
          cards: cards,
          page: 0,
          filter: true  
        });
      }
      catch(err){
        console.log(err)
        }
    }


    goTop = () => {
      this.refs.listRef.scrollToOffset({x: 0, y: 0, animated: true});
    }
    
    handleChange = async (searchText) => {
      try{
        const cards = await getSingleCard(this.props.set, searchText, this.db)  
        this.setState({
          cards: cards,
          searchText: searchText,
          page: 0,
          filter: true
        })
        
      }
      catch(err){
        console.log(err)
      }
    }
  
    render() {
      console.log("rendering", this.state.cards.length, "cards");
      
      return (
          <View style={styles.container}>
            <FlatList
              ref="listRef"
              contentContainerStyle={styles.list}
              data={this.state.cards}
              keyExtractor={item => item.id}
              onEndReached={this.state.filter ? null : this.getAdditionalData}
              onEndReachedThreshold={0.8}
              ListHeaderComponent={
                <View>
                  <HeaderButtons 
                    getCards={this.getCards} 
                  />
                  <SearchInput handleChange={this.handleChange} />
                </View>}
              renderItem={({item}) => {
                return (
                  <TouchableHighlight style={{alignItems: "center"}} onPress={console.log}>
                    <Image  style={styles.image} source={{ uri: item.image_url_png }} />
                  </TouchableHighlight>
                );
              }}
            />
            <Icon style={{
              color:"#DE6117", 
              position: "absolute", 
              bottom:0,
              right:0,
              margin:15
            }} 
              size={80}
              name="ios-arrow-dropup-circle"
              onPress={this.goTop} />
          </View>
      );
    }
  }
export default CardList;
