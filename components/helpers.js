import React from "react";
import {View, Image, Text, TouchableHighlight} from 'react-native';
import styles from '../styles';
import { SearchBar } from 'react-native-elements';

  export const HeaderButtons = ({getCards}) => {
    console.log();
    return(
        <View style={{flex:1, flexDirection: "row", alignItems: "center", justifyContent: "center"}}>
          <TouchableHighlight onPress={() => getCards("R")}>
            <Image source={{uri: "https://vignette.wikia.nocookie.net/magic-the-gathering/images/c/ce/Mana_R.png/revision/latest?cb=20150812090951&path-prefix=de"}} style={styles.mana} />
          </TouchableHighlight>
          <TouchableHighlight onPress={() => getCards("B")}>
            <Image source={{uri: "https://vignette.wikia.nocookie.net/magic-the-gathering/images/a/a6/Mana_B.png/revision/latest?cb=20150812090938&path-prefix=de"}} style={styles.mana}/>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => getCards("U")}>
            <Image source={{uri: "https://vignette.wikia.nocookie.net/magic-the-gathering/images/a/a8/Mana_U.png/revision/latest?cb=20150812090923&path-prefix=de"}} style={styles.mana}/>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => getCards("G")}>
            <Image source={{uri: "https://vignette.wikia.nocookie.net/magic-the-gathering/images/f/f7/Mana_G.png/revision/latest?cb=20150812091005&path-prefix=de"}} style={styles.mana}/>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => getCards("W")}>
            <Image source={{uri: "https://vignette.wikia.nocookie.net/magic-the-gathering/images/d/da/Mana_W.png/revision/latest?cb=20150812090910&path-prefix=de"}} style={styles.mana}/>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => getCards("")}>
            <Image source={{uri: "https://vignette.wikia.nocookie.net/magicduels/images/1/18/Mana_C.png/revision/latest?cb=20160225204219"}} style={styles.mana}/>
          </TouchableHighlight>
        </View>
    )
  }

  export const SearchInput = ({handleChange}) => {
    return(
      <View style={{flexDirection: "row", alignItems: "center", justifyContent: "center"}}>
        <SearchBar placeholder='search here...' round showLoading inputStyle={{color: "white"}} 
          containerStyle={{width: 350, marginTop: 10, backgroundColor: "#DE6117", borderTopWidth: 0, borderRightWidth: 0, borderRadius: 25}} 
          onChangeText={(text) => handleChange(text)}/>
      </View>
    )
  }