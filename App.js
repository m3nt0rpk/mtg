import React, {Component} from "react";
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  ScrollView
} from "react-native";
import {
  createStackNavigator,
  createAppContainer,
  createDrawerNavigator,
  DrawerItems, 
  SafeAreaView
} from "react-navigation";
import styles from './styles'
import Icon from "@expo/vector-icons/Ionicons";
import Home from './components/Home.js'
import CardList from './components/CardList.js';

const CustomDrawerContentComponent = (props) => (
  <ScrollView style={styles.drawer}>
    <SafeAreaView>
      <DrawerItems {...props} style={styles.drawerItems} />
    </SafeAreaView>
  </ScrollView>
);

class Grn extends Component{
  render(){
    return <CardList set="grn" />
  }
}

class M19 extends Component{
  render(){
    return <CardList set="m19" />
  }
}
class Dom extends Component{
  render(){
    return <CardList set="dom" />
  }
}

class Rix extends Component{
  render(){
    return <CardList set="rix" />
  }
}

class Xln extends Component{
  render(){
    return <CardList set="xln" />
  }
}

const DrawerNavigator = createDrawerNavigator({
  Home: {
    screen: Home
  },
  GuildsOfRavnica: {
    screen: Grn
  },
  CoreSet2019: {
    screen: M19
  },
  Dominaria: {
    screen: Dom
  },
  RivalsOfIxalan: {
    screen: Rix
  },
  Ixalan: {
    screen: Xln
  }
},{
  contentComponent: CustomDrawerContentComponent,
  contentOptions: {
    inactiveTintColor: "white",
    activeTintColor: "#DE6117"
  }
});

const StackNavigator = createStackNavigator(
  {
    DrawerNavigator: {
      screen: DrawerNavigator
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      const {routeName} = navigation.state.routes[navigation.state.index];
      return {
        headerLeft: (
          <Icon
            name="md-menu"
            size={30}
            style={{ marginLeft: 15, color: "white" }}
            onPress={() => navigation.toggleDrawer()}
          />
        ),
        headerRight: (
          <TouchableHighlight>
            <Image 
              style={{width: 35, height: 35, marginRight: 10}}
              source={{uri: "https://deckmaster.info/images/sets/PPR_C.png"}}
            />
          </TouchableHighlight>
        ),
        headerTitle: routeName,
        headerTintColor: "white",
        headerStyle: {
          backgroundColor: 'black',
         }
      };
    },
  }
);

export default createAppContainer(StackNavigator);

