import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#322f37",
      alignItems: "center",
      justifyContent: "center"
    },
    home:{
      flex: 1,
      backgroundColor: "#322f37",
      alignItems: "center",
    },
    image: {
      height: 460,
      width: 330,
      marginTop: 20,
    },
    button: {
      width: 100
    },
    text: {
      color: "white"
    },
    drawer: {
      flex: 1,
      backgroundColor: "#322f37"
    },
    drawerItems: {
      color: "white"
    },
    logo:{
      marginTop:30,
      height: 120,
      width: 400
    },
    homeText:{
      margin:30,
      fontSize: 20,
      color: "white"
    },
    mana:{
      width: 40,
      height: 40,
      marginTop: 10,
      marginBottom:0,
      marginLeft: 5,
      marginRight:5
    },
    goldfish: {
        width: 200,
        height: 200
    }
  });

  export default styles;
  