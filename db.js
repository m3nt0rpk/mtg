import {FileSystem, Asset, SQLite} from 'expo';

export async function openDb() {
    const asset = Asset.fromModule(require("./assets/mtg.db"));
    const dbUri = asset.uri;
    //await FileSystem.makeDirectoryAsync(`${FileSystem.documentDirectory}SQLite`);
    const response = await FileSystem.downloadAsync(dbUri, `${FileSystem.documentDirectory}SQLite/mtg.db`);    
    console.log("Got response", response);
    return SQLite.openDatabase("mtg.db");
  }


  export async function dbGetPromise(sql, params, db) {
    return new Promise((resolve, reject) => {
        db.transaction(tx => {
            tx.executeSql(sql, params, (_ ,{rows}) => {
              const cards = rows._array;
              resolve(cards)
            }, (_ ,{error}) => {
                reject(error)
            });
          });
    });
  }

  export async function getCards(set, color, db){
    return dbGetPromise("select * from cards where set_name = ? and colors = ? order by converted_mana_cost", [set, color], db);
  }

  export async function getCardsOnMount(set, page, db){
      return dbGetPromise("select * from cards where set_name = ? order by name limit 50 offset ?", [set, page], db);
  }

  export async function getSingleCard(set, cardName, db){
    return dbGetPromise("select * from cards where set_name = ? and instr(name, ?) limit 50", [set, cardName], db);
  }
